<?php

class NewOrder
{
    public $amount = 0;

    public $quantity;

    public $unitPrice;

    /**
     * Payment Gateway dependency
     * @var PaymentGateway
     */
    protected $gateway;

    public function __construct(int $quantity, float $unitPrice)
    {
        $this->quantity = $quantity;
        $this->unitPrice = $unitPrice;

        $this->amount = $quantity * $unitPrice;
    }

    public function process(PaymentGateway $gateway)
    {
        $gateway->charge($this->amount);
    }
}