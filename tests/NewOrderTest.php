<?php

use Mockery;
use PHPUnit\Framework\TestCase;

class NewOrderTest extends TestCase
{

    public function tearDown() : void
    {
        Mockery::close();
    }

    public function testOrderIsProcessedUsingMockery()//Arrange, Expect, Act, Assert
    {
        $order = new NewOrder(3, 1.99);

        $gatewayMock = Mockery::mock('PaymentGateway');

        $gatewayMock->shouldReceive('charge')
                    ->once()
                    ->with(5.97);
        
        $order->process($gatewayMock);
        
        $this->assertEquals(5.97, $order->amount);
    }

    public function testOrderIsProcessedUsingASpy() //http://docs.mockery.io/en/latest/reference/spies.html //Arrange, Act, Assert, Expect
    {
        $order = new NewOrder(3, 1.99);

        $gatewayMock = Mockery::spy('PaymentGateway');
        
        $order->process($gatewayMock);
        
        $this->assertEquals(5.97, $order->amount);

        $gatewayMock->shouldHaveReceived('charge')
                    ->once()
                    ->with(5.97);
    }
}