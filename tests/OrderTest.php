<?php

use Mockery;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{

    public function tearDown() : void
    {
        Mockery::close();
    }

    public function testOrderIsProcessed()
    {
        $gateway = $this->getMockBuilder('PaymentGateway') //Use getMockBuilder when testing with a dependency that doesn't exist
                        ->setMethods(['charge'])
                        ->getMock();
        
        $gateway->expects($this->once())
                ->method('charge')
                ->with($this->equalTo(200))
                ->willReturn(true);

        $order = new Order($gateway);

        $order->amount = 200;

        $this->assertTrue($order->process());
    }

    public function testOrderIsProcessedUsingMockery() //Same as above but Mockery reads better
    {

        $gateway = Mockery::mock('PaymentGateway');

        $gateway->shouldReceive('charge')
                ->once()
                ->with(200)
                ->andReturn(true);

        $order = new Order($gateway);

        $order->amount = 200;

        $this->assertTrue($order->process());
    }
}