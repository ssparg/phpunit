<?php

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{

    public function testReturnsFullName()
    {
        $user = new User;

        $user->first_name = 'Shaun';
        $user->surname = 'Sparg';

        $this->assertEquals('Shaun Sparg', $user->getFullName());
    }

    public function testFullNameIsEmptyByDefault()
    {
        $user = new User;

        $this->assertEquals('', $user->getFullName());
    }

    public function testNotificationIsSent()
    {
        $user = new User;

        $mockMailer = $this->createMock(Mailer::class);

        $mockMailer->expects($this->once())
                   ->method('sendMessage')
                   ->with($this->equalTo('shaun@test.com'), $this->equalTo('hello'))
                   ->willReturn(true);
        
        $user->setMailer($mockMailer);

        $user->email = 'shaun@test.com';

        $this->assertTrue($user->notify('hello'));
    }

    public function testCannotNotifyUserWithNoEmail()
    {
        $user = new User;

        //$mockMailer = $this->createMock(Mailer::class);

        $mockMailer = $this->getMockBuilder(Mailer::class)
                           ->setMethods(null) // Pass an array of methods you want to stub.
                           ->getMock();// Same as createMock but you now have options available.

        $user->setMailer($mockMailer);

        $this->expectException(Exception::class);

        $user->notify('hello');
    }
}